{ config, pkgs, lib, ... }:

with lib;

let
  secrets = import ./secrets.nix;

  tz = "Europe/London";

  # Escape $ (with $$) in strings so docker does not interpret them as
  # environment variables
  escapeDockerCompose = builtins.replaceStrings [ "$" ] [ "$$" ];

  # Common service options
  serviceDefaults = {
    service = {
      restart = "unless-stopped";
      environment = {
        PUID = secrets.PUID;
        PGID = secrets.PGID;
        TZ = tz;
      };
      tmpfs = mkDefault [
        "/run"
        "/tmp:exec,mode=777"
      ]; # low priority as NixOS-based containers will override this
      labels = {
        "traefik.enable" = mkDefault "false";
      }; # disable træfik by default
    };
    out.service.dns_search = [ "dns.podman" ];
  };

  # Common NixOS-based service options
  nixosServiceDefaults = {
    service = { useHostStore = true; };
    nixos = {
      useSystemd = true;
      configuration = {
        environment.noXlibs = true;
        boot = {
          tmp.useTmpfs = true;
          isContainer = true;
        };
        time.timeZone = tz;
        i18n.defaultLocale = "en_GB.UTF-8";
        system.stateVersion = "21.05";
        services.nscd.enableNsncd = true;
      };
    };
  };

  mkService = cfg: mkMerge [ serviceDefaults cfg ];

  mkNixosService = cfg: mkMerge [ serviceDefaults nixosServiceDefaults cfg ];

  # Function to generate træfik labels for containers
  mkTraefikLabels =
    { service, port, auth ? true, host ? "${service}.${secrets.domain}" }:
    {
      "traefik.enable" = "true";
      "traefik.http.routers.${service}.rule" = "Host(`${host}`)";
      "traefik.http.services.${service}.loadbalancer.server.port" =
        toString port;
      "traefik.http.routers.${service}.tls.certresolver" = "le";
      "traefik.http.routers.${service}.entrypoints" = "websecure";
      "traefik.http.middlewares.compression.compress" = "true";
      "traefik.http.routers.${service}.middlewares" = "compression";
    } // optionalAttrs auth {
      "traefik.http.routers.${service}.middlewares" = "auth,compression";
      "traefik.http.middlewares.auth.basicauth.users" =
        "${secrets.http.user}:${escapeDockerCompose secrets.http.pass}";
    };

in
{
  config = {
    project.name = "matrix";

    docker-compose.raw.volumes = {
      traefik = { };
      atuin = { };
      thelounge = { };
      rss-db = { };
      rss-db-backup = { };
      wallabag-images = { };
      matrix-db = { };
      matrix-db-backup = { };
      synapse = { };
      syncthing = { };
      kopia-sftp = { };
      ledger = { };
    };

    services = {

      traefik = mkService {
        service =
          let
            sftpPort = 2222;
            sftpPortStr = toString sftpPort;
          in
          {
            image = "traefik:latest";
            container_name = "traefik";
            command = [
              "--entrypoints.web.address=:80"
              "--entrypoints.websecure.address=:443"
              "--entrypoints.sftp.address=:${sftpPortStr}/tcp"
              "--providers.docker"
              "--providers.file.filename=/etc/traefik/dynamic.yml"
              "--api"
              "--api.dashboard=true"
              "--certificatesresolvers.le.acme.email=xx.acme@themaw.xyz"
              "--certificatesresolvers.le.acme.storage=/config/acme.json"
              "--certificatesresolvers.le.acme.tlschallenge=true"

              "--accessLog.filePath=/var/log/access.log"
              "--accessLog.filters.statusCodes=400-499"
              # global https redirect
              "--entrypoints.web.http.redirections.entryPoint.to=websecure"
              "--entrypoints.web.http.redirections.entryPoint.scheme=https"
            ];
            ports = [ "80:80" "443:443" "${sftpPortStr}:${sftpPortStr}" ];
            volumes = [
              "traefik:/config" # acme.json store
              rec {
                type = "bind";
                source = "/var/run/docker.sock";
                target = source;
                read_only = true;
              }
              "${
              builtins.toFile "traefik-dynamic.yml" (builtins.toJSON {
                tls.options.default = {
                  minVersion = "VersionTLS13";
                  sniStrict = true;
                };
              })
            }:/etc/traefik/dynamic.yml:ro"
            ];
            labels = {
              "traefik.enable" = "true";
              "traefik.http.routers.dashboard.rule" =
                "Host(`traefik.${secrets.domain}`)";
              "traefik.http.routers.dashboard.service" = "api@internal";
              "traefik.http.routers.dashboard.middlewares" = "auth,compression";
              "traefik.http.middlewares.compression.compress" = "true";
              "traefik.http.middlewares.auth.basicauth.users" =
                "${secrets.http.user}:${escapeDockerCompose secrets.http.pass}";
              "traefik.http.routers.dashboard.tls.certresolver" = "le";
              "traefik.http.routers.dashboard.entrypoints" = "websecure";
            };
          };
      };

      ## Syncthing
      syncthing =
        let webUiPort = 8384;
        in
        mkNixosService {
          nixos = {
            configuration = { config, ... }: {
              services.syncthing = {
                enable = true;
                guiAddress = "0.0.0.0:${toString webUiPort}";
              };
              systemd.services.fix-syncthing-permissions =
                with config.services.syncthing; {
                  description = "Set permissions on syncthing's dataDir";
                  requiredBy = [ "syncthing.service" ];
                  before = [ "syncthing.service" ];
                  serviceConfig = {
                    ExecStart =
                      "${pkgs.coreutils}/bin/chown -R ${user}:${group} ${dataDir}";
                    Type = "oneshot";
                    RemainAfterExit = true;
                  };
                };
            };
          };
          service = rec {
            container_name = "syncthing";
            ports = [ "22000:22000" "21027:21027/udp" ];
            volumes = [
              "syncthing:/var/lib/syncthing"
              "ledger:/var/lib/syncthing/ledger"
            ];
            labels = mkTraefikLabels {
              service = "syncthing";
              port = webUiPort;
            };
          };
        };

      ## Fava
      fava = mkNixosService {
        nixos = {
          configuration = {
            systemd.services.fava = {
              description = "Fava Web UI for Beancount";
              after = [ "network.target" ];
              wantedBy = [ "multi-user.target" ];
              serviceConfig = {
                Type = "simple";
                ExecStart =
                  "${pkgs.fava}/bin/fava --host 0.0.0.0 /ledger/ledger.beancount";
              };
            };
          };
        };
        service = rec {
          container_name = "fava";
          volumes = [ "ledger:/ledger" ];
          labels = mkTraefikLabels {
            service = container_name;
            port = 5000;
          };
        };
      };

      kopia-sftp = mkService {
        service = with secrets.kopia; {
          image = "atmoz/sftp:alpine";
          container_name = "kopia-sftp";
          volumes =
            [ "kopia-sftp:/etc/ssh" "/mnt/nas/kopia:/home/${user}/kopia" ];
          command =
            let
              kopiaUid = 5273;
              kopiaGid = 65555;
            in
            "${user}:${escapeDockerCompose pass}:e:${toString kopiaUid}:${
            toString kopiaGid
          }";
          labels = {
            "traefik.enable" = "true";
            "traefik.tcp.routers.sftp.rule" = "HostSNI(`*`)";
            "traefik.tcp.routers.sftp.entrypoints" = "sftp";
            "traefik.tcp.routers.sftp.service" = "sftp";
            "traefik.tcp.services.sftp.loadbalancer.server.port" = "22";
          };
        };
      };

      ## IRC stuff

      # Lounge users will need to be manually added
      thelounge = mkNixosService {
        nixos = {
          configuration = {
            services.thelounge = {
              enable = true;
              plugins = with pkgs.theLoungePlugins; [ themes.zenburn ];
              extraConfig = {
                prefetch = true;
                prefetchStorage = true;
                prefetchMaxImageSize = 10240;
                maxHistory = 5000;
                leaveMessage = ":x";
                identd = {
                  enable = true;
                  port = 9001;
                };
              };
            };
          };
        };
        service = rec {
          container_name = "thelounge";
          volumes = [
            "thelounge:/var/lib/thelounge" # logs etc.
          ];
          labels = mkTraefikLabels {
            service = container_name;
            port = 9000;
            auth = false;
          };
        };
      };

      ## RSS stuff

      redis = mkNixosService {
        nixos = {
          configuration = {
            services.redis = {
              servers."" = {
                enable = true;
                bind = "0.0.0.0";
                settings.protected-mode = "no";
              };
              # vmOverCommit = true; # no effect in container
            };
          };
        };

        service.container_name = "redis";
      };

      # https://github.com/DIYgod/RSSHub/blob/master/docker-compose.yml
      chrome-headless = mkService {
        service = {
          image = "browserless/chrome:1.43-chrome-stable";
          container_name = "chrome-headless";
        };
      };

      rsshub = mkService {
        service = {
          image = "diygod/rsshub";
          container_name = "rsshub";
          environment = with secrets.rsshub; {
            PORT = 80;
            NODE_ENV = "production";
            CACHE_TYPE = "redis";
            REDIS_URL = "redis://redis:6379/";
            PUPPETEER_WS_ENDPOINT = "ws://browserless:3000";

            # Twitter API access
            TWITTER_CONSUMER_KEY = twitter.consumer-key;
            TWITTER_CONSUMER_SECRET = twitter.consumer-key-secret;
            TWITTER_TOKEN_lunik1 = with twitter;
              "${consumer-key},${consumer-key-secret},${access-token},${access-token-secret}";

            # Instagram hub
            IG_USERNAME = instagram.user;
            IG_PASSWORD = instagram.pass;
          };
          depends_on = [ "redis" "chrome-headless" ];
        };
      };

      mercury = mkService {
        service = {
          image = "wangqiru/mercury-parser-api";
          container_name = "mercury";
        };
      };

      rss-db = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [ postgresql_14 ];
            i18n.defaultLocale = mkForce "en_US.UTF-8";
            services = {
              postgresql = {
                enable = true;
                package = pkgs.postgresql_14;
                enableTCPIP = true;
                initialScript = with secrets;
                  pkgs.writeText "postgres-init.sql" ''
                    -- tt-rss DB
                    CREATE ROLE "${tt-rss.db.user}" WITH LOGIN PASSWORD '${tt-rss.db.pass}';
                    CREATE DATABASE "${tt-rss.db.name}" WITH OWNER "${tt-rss.db.user}";

                    -- wallabag DB
                    CREATE ROLE "${wallabag.db.user}" WITH LOGIN PASSWORD '${wallabag.db.pass}';
                    CREATE DATABASE "${wallabag.db.name}" WITH OWNER "${wallabag.db.user}";
                  '';
                authentication = mkForce ''
                  # TYPE  DATABASE        USER            ADDRESS                 METHOD
                  # localhost
                  local   all             all                                     trust
                  host    all             all             127.0.0.1/32            trust
                  host    all             all             ::1/128                 trust
                  # podman containers
                  host    all             all             10.0.0.0/8              trust
                '';
              };
              postgresqlBackup = {
                enable = true;
                startAt = "02:00:00";
                databases = [ "tt-rss" "wallabag" ];
                compression = "zstd";
              };
            };
          };
        };

        service = {
          container_name = "rss-db";
          volumes = with config.nixos.evaluatedConfig.services; [
            "rss-db:${postgresql.dataDir}"
            "rss-db-backup:${postgresqlBackup.location}"
          ];
        };
      });

      # podman exec wallabag /var/www/wallabag/bin/console wallabag:install --env=prod --no-interaction
      # must be run when first setting up wallabag
      wallabag = mkService {
        service = rec {
          image = "wallabag/wallabag";
          container_name = "wallabag";
          environment = with secrets; {
            POSTGRES_USER = wallabag.db.name;
            POSTGRES_PASSWORD = wallabag.db.user;
            SYMFONY__ENV__DATABASE_HOST = "rss-db";
            SYMFONY__ENV__DATABASE_DRIVER = "pdo_pgsql";
            SYMFONY__ENV__DATABASE_PORT = 5432;
            SYMFONY__ENV__DATABASE_NAME = wallabag.db.name;
            SYMFONY__ENV__DATABASE_USER = wallabag.db.user;
            SYMFONY__ENV__DATABASE_PASSWORD = wallabag.db.pass;
            SYMFONY__ENV__DOMAIN_NAME = "https://wallabag.${secrets.domain}";
          };
          volumes = [ "wallabag-images:/var/www/wallabag/web/assets/images" ];
          depends_on = [ "rss-db" "redis" ];
          labels = mkTraefikLabels {
            service = container_name;
            auth = false;
            port = 80;
          };
        };
      };

      tt-rss = mkService {
        service = rec {
          image = "wangqiru/ttrss";
          container_name = "tt-rss";
          environment = with secrets; {
            DB_HOST = "rss-db";
            DB_PORT = 5432;
            CHECK_FOR_UPDATES = "false";
            SELF_URL_PATH = "https://tt-rss.${domain}/";
            SINGLE_USER_MODE = "true";
            DB_NAME = tt-rss.db.name;
            DB_USER = tt-rss.db.user;
            DB_PASS = tt-rss.db.pass;
            ENABLE_PLUGINS = "auth_internal,wallabag_v2";
          };
          depends_on = [ "rss-db" "rsshub" ];
          labels = mkTraefikLabels {
            service = container_name;
            port = 80;
          };
        };
      };

      ## Matrix stuff

      nginx = mkNixosService {
        nixos = {
          configuration = {
            services.nginx = {
              enable = true;
              recommendedTlsSettings = true;
              recommendedOptimisation = true;
              recommendedGzipSettings = true;
              recommendedProxySettings = true;
              # user = "root";
              virtualHosts."${secrets.domain}".locations."/".root = "/www";
            };
            systemd.tmpfiles.rules = [
              "L+ /www/.well-known/matrix/server - - - - ${
                builtins.toFile "server" (builtins.toJSON {
                  "m.server" = "synapse.${secrets.domain}:443";
                })
              }"
              "L+ /www/.well-known/matrix/client - - - - ${
                builtins.toFile "client" (builtins.toJSON {
                  "m.homeserver".base_url = "https://${secrets.domain}";
                })
              }"
            ];
          };
        };

        service = rec {
          container_name = "nginx";
          labels = mkTraefikLabels {
            service = container_name;
            auth = false;
            host = secrets.domain;
            port = 80;
          };
        };
      };

      matrix-db = mkNixosService {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [ postgresql_13 ];
            i18n.defaultLocale = mkForce "en_US.UTF-8";
            services = {
              postgresql = {
                enable = true;
                package = pkgs.postgresql_13;
                enableTCPIP = true;
                port = 5432;
                initialScript = with secrets;
                  pkgs.writeText "postgres-init.sql" ''
                    -- Synapse DB
                    CREATE ROLE "${synapse.db.user}" WITH LOGIN PASSWORD '${synapse.db.pass}';
                    CREATE DATABASE "${synapse.db.name}" WITH OWNER "${synapse.db.user}"
                      TEMPLATE template0
                      LC_COLLATE = "C"
                      LC_CTYPE = "C";
                  '';
                authentication = mkForce ''
                  # Generated file; do not edit!
                  # Trust local connections (IPv4 and IPv6)
                  # TYPE  DATABASE        USER            ADDRESS                 METHOD
                  local   all             all                                     trust
                  host    all             all             127.0.0.1/32            trust
                  host    all             all             ::1/128                 trust
                  # and those from docker containers
                  host    all             all             172.16.0.0/12           trust
                  # and those from podman containers
                  host    all             all             10.0.0.0/8              trust
                '';
              };
              postgresqlBackup = {
                enable = true;
                startAt = "02:00:00";
                databases = [ "synapse" ];
                compression = "zstd";
              };
            };
          };
        };

        service = {
          container_name = "matrix-db";
          volumes = [
            "matrix-db:/var/lib/postgresql/13"
            "matrix-db-backup:/var/backup/postgresql"
          ];
          labels = { "traefik.enable" = "false"; };
        };
      };

      synapse =
        let dataDir = "/data";
        in mkNixosService {
          nixos = {
            configuration = {
              environment.systemPackages = with pkgs; [ matrix-synapse ];
              environment.noXlibs = lib.mkForce false;
              services.matrix-synapse = {
                inherit dataDir;
                enable = true;
                withJemalloc = true;
                settings = {
                  server_name = secrets.domain;
                  public_baseurl = "https://synapse.${secrets.domain}";
                  max_upload_size = "100M";
                  max_image_pixels = "64M";
                  listeners = [{
                    port = 8008;
                    type = "http";
                    tls = false;
                    x_forwarded = true;
                    bind_addresses = [ "0.0.0.0" ];
                    resources = [{
                      names = [ "client" "federation" ];
                      compress = false;
                    }];
                  }];
                  enable_registration = false; # true for open registration
                  database.args = with secrets.synapse.db; {
                    user = user;
                    password = pass;
                    database = name;
                    host = config.services.matrix-db.service.container_name;
                  };
                  experimenal_features.spaces_enabled = true;
                  suppress_key_server_warning = true;
                  trusted_key_servers = [
                    {
                      server_name = "matrix.org";
                      verify_keys = { "ed25519:auto" = "Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw"; };
                    }
                    {
                      server_name = "nixos.org";
                      verify_keys = { "ed25519:j8tsLm" = "ysJrOC8kica9QA/fOCQT/lHJvcyCDnr1lCvXN0wsxwA"; };
                    }
                    {
                      server_name = "mozilla.org";
                      verify_keys = { "ed25519:0" = "RsDggkM9GntoPcYySc8AsjvGoD0LVz5Ru/B/o5hV9h4"; };
                    }
                  ];
                  use_presence = false;
                  log_config = builtins.toFile "log-config.yaml" ''
                    version: 1
                    formatters:
                        journal_fmt:
                            format: '%(name)s: [%(request)s] %(message)s'
                    filters:
                        context:
                            (): synapse.util.logcontext.LoggingContextFilter
                            request: ""
                    handlers:
                        journal:
                            class: systemd.journal.JournalHandler
                            formatter: journal_fmt
                            filters: [context]
                            SYSLOG_IDENTIFIER: synapse
                    root:
                        level: WARNING
                        handlers: [journal]
                    disable_existing_loggers: False
                  '';
                };
              };

              ## Needed when registering new users, see Chap 24 of NixOS manual
              ## TLDR: uncomment lines below and use
              ## docker exec -it synapse register_new_matrix_user -k <registration_shared_secret> http://localhost:8008
              ## or for open registration set enable_registration above to true
              # configuration.environment.systemPackages = [ pkgs.matrix-synapse ];
              # configuration.services.matrix-synapse.registration_shared_secret =
              #   secrets.synapse.registration_shared_secret;
            };
          };

          service = rec {
            container_name = "synapse";
            volumes = [ "synapse:${dataDir}" ];
            labels = mkTraefikLabels {
              service = container_name;
              auth = false;
              port = 8008;
            };
            depends_on = [ "nginx" "matrix-db" ];
          };
        };

        atuin =
          let port = 8888;
          in
          mkNixosService {
        nixos = {
          configuration = {
            environment.noXlibs = lib.mkForce false;
            services.atuin = {
              inherit port;
              host = "0.0.0.0";
              enable = true;
              openFirewall = true;
              openRegistration = false;
            };
          };
        };
        service = rec {
          capabilities.SYS_ADMIN = true;
          container_name = "atuin";
          volumes = [
            "atuin:/var/lib/postgresql"
          ];
          labels = mkTraefikLabels {
            service = container_name;
            inherit port;
            auth = false;
          };
        };
      };
    };
  };
}
